package br.com.lp3.apifornecedor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public class ProdutoController {
	
	@Autowired
	private ProdutoService produtoService;
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<ProdutoDTO> findProdutoById(@PathVariable Long id) {
		ProdutoDTO produtoDTO = produtoService.findById(id);
		if (produtoDTO != null) {
			return new ResponseEntity<ProdutoDTO>(produtoDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<ProdutoDTO>(prdutoDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.POST)
	public ResponseEntity<ProdutoDTO> saveProduto(@RequestBody ProdutoDTO produtoDTO) {
		produtoDTO = produtoService.saveProduto(produtoDTO);
		if (produtoDTO != null) {
			return new ResponseEntity<ProdutoDTO>(produtoDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<ProdutoDTO>(produtoDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	public ProdutoDTO delete(ProdutoDTO produtoDTO) {
		Produto produto = new Produto(produtoDTO.getId(), produtoDTO.getNome(), produtoDTO.getDescricao(),produtoDTO.getValor(),produtoDTO.getFornecedor());
	    produtoRepository.delete(produto);
	    return produtoDTO;
	   }
	
	public List<ProdutoDTO> findAll() {
		Iterable <Produto> iProdutos = produtoRepository.findAll();
		List<ProdutoDTO> ps = new ArrayList<ProdutoDTO>();
			for(Produto p : iProdutos) {
				ProdutoDTO pDTO = new ProdutoDTO();
				pDTO.setId(p.getId());
				pDTO.setNome(p.getNome());
				pDTO.setDescricao(p.getDescricao());
				pDTO.setValor(p.getValor());
				pDTO.setFornecedor(p.getFornecedor());
			}
			
			return ps;
		}

	public ProdutoDTO update(ProdutoDTO produtoDTO) {
		Produto produto = new Produto(produtoDTO.getId(), produtoDTO.getNome(), produtoDTO.getDescricao(),produtoDTO.getValor(),produtoDTO.getFornecedor());
	    produto = produtoRepository.update(produto);
	    produtoDTO.setId(produto.getId());
	    return produtoDTO;
	   }
}