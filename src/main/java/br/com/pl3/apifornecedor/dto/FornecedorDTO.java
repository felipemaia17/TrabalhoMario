package br.com.lp3.apifornecedor.dto;

public class FornecedorDTO {

	int id;
	String nome;
	String endereco;
	long cpnj;
	String razao_social;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public long getCpnj() {
		return cpnj;
	}
	public void setCpnj(long cpnj) {
		this.cpnj = cpnj;
	}
	public String getRazao_social() {
		return razao_social;
	}
	public void setRazao_social(String razao_social) {
		this.razao_social = razao_social;
	}
	
	
}
